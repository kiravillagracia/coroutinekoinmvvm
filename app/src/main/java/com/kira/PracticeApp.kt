package com.kira

import android.app.Application
import com.kira.core.modules.apiModule
import com.kira.core.modules.repositoryModule
import com.kira.core.modules.retrofitModule
import com.kira.core.modules.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class PracticeApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@PracticeApp)
            modules(
                viewModelModule,
                repositoryModule,
                retrofitModule,
                apiModule
            )
        }
    }
}