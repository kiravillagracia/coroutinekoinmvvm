package com.kira.core.modules

import com.kira.beer.BeerApi
import com.kira.beer.BeerRepo
import org.koin.dsl.module

val repositoryModule = module {
    factory { provideBeerRepo(get()) }
}

fun provideBeerRepo(beerApi: BeerApi) = BeerRepo(beerApi)