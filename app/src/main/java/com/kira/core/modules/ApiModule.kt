package com.kira.core.modules

import com.kira.beer.BeerApi
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    factory { provideOtpApi(get()) }
}

fun provideOtpApi(retrofit: Retrofit): BeerApi = retrofit.create(BeerApi::class.java)