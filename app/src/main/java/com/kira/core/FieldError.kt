package com.kira.core

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class FieldError(
    @SerializedName("field")
    val field: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("defaultMessage")
    val defaultMessage: String
): Serializable