package com.kira.core

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ApiError(
    @SerializedName("message")
    val message: String,
    @SerializedName("detail")
    val detail: ErrorDetails?,
    @SerializedName("code")
    val code: String?
): Serializable