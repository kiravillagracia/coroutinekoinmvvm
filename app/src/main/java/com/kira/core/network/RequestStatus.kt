package com.kira.core.network

enum class RequestStatus {
    SUCCESS,
    ERROR,
    LOADING
}