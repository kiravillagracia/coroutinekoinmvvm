package com.kira.core

object CoreConstants {

    const val CONNECT_TIMEOUT = 60L
    const val READ_TIMEOUT = 60L
    const val WRITE_TIMEOUT = 60L
    const val SIXTY_SECONDS = 60000L
    const val ONE_SECOND = 1000L
    const val MAX_IMAGE_WIDTH_PX = 1280
    const val MAX_FILE_SIZE_MB = 3
}