package com.kira.core

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ErrorDetails(
    @SerializedName("non_field_errors")
    val nonFieldErrors: List<String>? = listOf(),
    @SerializedName("field_errors")
    val fieldErrors: List<String>? = listOf()
): Serializable