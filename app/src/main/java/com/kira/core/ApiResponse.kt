package com.kira.core

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ApiResponse<T>(
    @SerializedName("data")
    val data: T? = null,
    @SerializedName("error")
    val error: ApiError? = null,
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("fieldErrors")
    val fieldErrors: List<FieldError>? = null
): Serializable {

    fun hasErrors(): Boolean = error != null || fieldErrors != null

}