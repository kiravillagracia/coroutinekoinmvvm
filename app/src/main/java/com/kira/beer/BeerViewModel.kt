package com.kira.beer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.kira.core.BaseViewModel
import com.kira.core.UIEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.component.inject

class BeerViewModel : BaseViewModel() {

    private val beerRepo: BeerRepo by inject()

    private val _events = MutableLiveData<UIEvent>()
    val events: LiveData<UIEvent> = _events

    fun getAllBeers() {
        _events.value = BeerEvent.OnStartLoading(true)
        viewModelScope.launch(Dispatchers.IO) {
            val result = kotlin.runCatching { beerRepo.getAllBeers() }
            withContext(Dispatchers.Main) {
                result.onSuccess { response ->
                    response?.let { beerList ->
                        if (beerList.isEmpty()) {
                            _events.value = BeerEvent.OnNoAvailable
                        } else {
                            _events.value = BeerEvent.OnFinishedLoading(beerList)
                        }
                    }
                }.onFailure { error ->
                    _events.value = BeerEvent.OnFailedFetching(error.localizedMessage)
                }
            }
        }
    }
}