package com.kira.beer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.kira.R
import com.kira.databinding.ListItemBeerBinding

class BeerListAdapter(
    private var beerList: List<Beer>
) : RecyclerView.Adapter<BeerListAdapter.ViewHolder>() {

    lateinit var binding: ListItemBeerBinding

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (itemCount > 0) {
            val beer = beerList[position]
            holder.apply {
                binding.descriptionTxt.text = beer.beerDescription
                binding.nameTxt.text = beer.beerName
                binding.imgBeer.load(beer.beerImageUrl) {
                    crossfade(true)
                    placeholder(R.drawable.ic_launcher_background)
                }
            }
        }
    }

    override fun getItemCount(): Int { return beerList.size }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_beer,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(
        val binding: ListItemBeerBinding
    ) : RecyclerView.ViewHolder(binding.root)
}