package com.kira.beer

class BeerRepo(private val beerApi: BeerApi) {

    suspend fun getAllBeers() = beerApi.getAllBeers().body()
}