package com.kira.beer

import retrofit2.Response
import retrofit2.http.GET

interface BeerApi {

    @GET("beers")
    suspend fun getAllBeers(): Response<List<Beer>>
}