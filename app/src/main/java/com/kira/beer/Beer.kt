package com.kira.beer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Beer(
    @SerializedName("id")
    val beerId: Long,
    @SerializedName("name")
    val beerName: String = "",
    @SerializedName("image_url")
    val beerImageUrl: String = "",
    @SerializedName("description")
    val beerDescription: String? = ""
): Serializable