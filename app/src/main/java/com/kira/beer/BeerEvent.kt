package com.kira.beer

import com.kira.core.UIEvent

sealed class BeerEvent : UIEvent {
    data class OnStartLoading(val success: Boolean) : BeerEvent()
    data class OnFailedFetching(val error: String) : BeerEvent()
    data class OnFinishedLoading(val beers: List<Beer>) : BeerEvent()
    object OnNoAvailable: BeerEvent()
}