package com.kira

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.kira.beer.Beer
import com.kira.beer.BeerEvent
import com.kira.beer.BeerListAdapter
import com.kira.beer.BeerViewModel
import com.kira.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val beerViewModel: BeerViewModel by viewModel()
    private lateinit var beerListAdapter: BeerListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        retrieveAllBeers()
        observeBeerRetrieval()
    }

    private fun retrieveAllBeers() {
        beerViewModel.getAllBeers()
    }

    private fun initRecyclerView(beerList: List<Beer>) {
        beerListAdapter = BeerListAdapter(beerList)
        binding.beerListRv.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = beerListAdapter
        }
    }

    private fun observeBeerRetrieval() {
        lifecycleScope.launchWhenCreated {
            beerViewModel.events.observe(this@MainActivity) { event ->
                when (event) {
                    is BeerEvent.OnFinishedLoading -> {
                        initRecyclerView(event.beers)
                    }
                    is BeerEvent.OnNoAvailable -> {
                        //no available tip
                    }
                    is BeerEvent.OnFailedFetching -> {
                        Log.e("ERROR", "error")
                    }
                }
            }
        }
    }
}