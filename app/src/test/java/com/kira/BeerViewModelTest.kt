package com.kira

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kira.beer.Beer
import com.kira.beer.BeerEvent
import com.kira.beer.BeerRepo
import com.kira.beer.BeerViewModel
import com.kira.core.UIEvent
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class BeerViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @MockK
    private lateinit var beerRepo: BeerRepo

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun `#retrieveBeers should pass 'OnSuccessFetching' when submitting an identifier and screen`() {
        val beers : List<Beer> = listOf()
        val eventsObserver = mockk<Observer<UIEvent>>().also {
            every {
                it.onChanged(any())
            } answers { }
        }
        testCoroutineRule.runBlockingTest {
            val viewModel = BeerViewModel()
            viewModel.events.observeForever(eventsObserver)

            coEvery {
                beerRepo.getAllBeers()
            } returns beers
            viewModel.getAllBeers()
            verify {
                eventsObserver.onChanged(
                    BeerEvent.OnStartLoading(true)
                )
            }
            viewModel.events.removeObserver(eventsObserver)
        }
    }

    @Test
    fun `#retrieveBeers should send 'OnFailed' when an error is encountered during retrieval`() {
        val errorMessage = "Error"
        val eventsObserver = mockk<Observer<UIEvent>>().also {
            every {
                it.onChanged(any())
            } answers { }
        }
        testCoroutineRule.runBlockingTest {
            val viewModel = BeerViewModel()
            viewModel.events.observeForever(eventsObserver)
            viewModel.getAllBeers()
            verify(exactly = 0) {
                eventsObserver.onChanged(BeerEvent.OnFailedFetching(errorMessage))
            }
            viewModel.events.removeObserver(eventsObserver)
        }
    }

    @Test
    fun `#retrieveBeers should send 'OnFinishedLoading' when finished fetching for data`() {
        val beers : List<Beer> = listOf()
        val eventsObserver = mockk<Observer<UIEvent>>().also {
            every {
                it.onChanged(any())
            } answers { }
        }
        testCoroutineRule.runBlockingTest {
            val viewModel = BeerViewModel()
            viewModel.events.observeForever(eventsObserver)
            viewModel.getAllBeers()
            verify(exactly = 0) { eventsObserver.onChanged(BeerEvent.OnFinishedLoading(beers)) }
            viewModel.events.removeObserver(eventsObserver)
        }
    }

    @Test
    fun `#retrieveBeers should send 'OnNoAvailable' when no tip is retrieved`() {
        val eventsObserver = mockk<Observer<UIEvent>>().also {
            every {
                it.onChanged(any())
            } answers { }
        }
        testCoroutineRule.runBlockingTest {
            val viewModel = BeerViewModel()
            viewModel.events.observeForever(eventsObserver)
            viewModel.getAllBeers()
            verify(exactly = 0) {
                eventsObserver.onChanged(BeerEvent.OnNoAvailable)
            }
            viewModel.events.removeObserver(eventsObserver)
        }
    }
}